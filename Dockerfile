# Dockerfile
# docker build -t ubuntu1604py36
FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y software-properties-common vim
RUN add-apt-repository ppa:jonathonf/python-3.6
RUN apt-get update

RUN apt-get install -y build-essential python3.6 python3.6-dev python3-pip python3.6-venv
RUN apt-get install -y git

# update pip
RUN python3.6 -m pip install pip --upgrade
RUN python3.6 -m pip install wheel

# Get source
# 1. from source folder

#CMD mkdir pond5_api
#COPY . /pond5_api

# 2. BETTER from git repository
RUN git clone https://bitbucket.org/andreicalistru/pond5_api.git

WORKDIR /pond5_api
CMD cd /pond5_api
RUN pip install -e .

# Run server and app
EXPOSE 5000
ENTRYPOINT ["python3.6"]
CMD ["pond5_api/rest.py"]