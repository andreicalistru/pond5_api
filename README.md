**Pond5 Rest API** v1.0

---
__Install & Run__:

1. `git clone https://bitbucket.org/andreicalistru/pond5_api.git`
2. `cd pond5_api`

Then you have two options to test the application:

1. To run the application from __Python__ run the following commands from a `terminal`:

    1.1. `pip instal -e .`

    1.2. `python pond5_api/rest.py`

    1.3. Follow step #3

2. To run the application from __Docker__ please execute the following commands:

    2.1. `docker build -t pond5-api .`

    2.2. `docker run -i -p 5000:5000 -d pond5-api`

    2.3. Follow step #3

3. To verify the application open a browser and access [this url](http://0.0.0.0:5000) or from terminal execute `curl -X GET http://0.0.0.0:5000/`

---
__Tests__

To run the tests suite, run `python -m unittest discover pond5_api/tests` command from a terminal.
*Expected to fail 2 tests*