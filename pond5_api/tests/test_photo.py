import unittest
from unittest.mock import patch
import requests_mock

from pond5_api.photo import *


class TestPhoto(unittest.TestCase):
    def setUp(self):
        self.photo = Photo(11497188)

    def test_raise_exceptions(self):
        """
        The http request to pond5 might cause many 400 bad request errors.
        :return: An exception is triggered in case of failure
        """
        with self.assertRaises(requests.exceptions.HTTPError):
            self.photo.set_photo_details()

    def test_response(self):
        """
        Test that the response returned is in a html format
        :return: html content
        """
        expected = '<html>goes over here</html>'
        with requests_mock.mock() as m:
            m.get(POND_DOMAIN.format(self.photo.photo_id), text=expected)
            result = self.photo.get_photo_response()
            self.assertEqual(result, bytes(expected, 'utf-8'))

    @patch('pond5_api.photo.Photo.get_media_info', return_value={'title': 'Test title'})
    def test_media_info(self, get_media_info):
        """
        Test if the get_media_info() is returning a dictionary with a `title` key
        :param get_media_info:
        :return: a dict with `title` key
        """
        self.assertEqual(get_media_info(), {'title': 'Test title'})

    @patch('pond5_api.photo.Photo.get_media_format_types', return_value={
        'format_types': {
            'L': {
                'resolution': '4388 x 2906',
                'size': ' 3.3MB',
                'width': '4388'
            },
            'M': {
                    'resolution': '2633 x 1743',
                    'size': '1.3MB',
                    'width': '2633'
            }
        }
    })
    def test_media_format_types(self, get_media_format_types):
        """
        Test if the get_media_format_types() is returning a dictionary with format types dictionaries
        :param get_media_format_types:
        :return: a dict with `format_types` available
        """
        self.assertEqual(get_media_format_types(), {
            'format_types': {
                'L': {
                    'resolution': '4388 x 2906',
                    'size': ' 3.3MB',
                    'width': '4388'
                },
                'M': {
                        'resolution': '2633 x 1743',
                        'size': '1.3MB',
                        'width': '2633'
                }
            }
        })


if __name__ == '__main__':
    unittest.main()
