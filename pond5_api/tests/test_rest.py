import unittest
from flask_testing import TestCase

from pond5_api import rest


class TestRestApi(TestCase):
    def create_app(self):
        """
        Creates the application object required on testing a Flask app
        :return: app object
        """
        app = rest.create_app()
        app.config['TESTING'] = True
        return app

    def test_rest_ping(self):
        """
        Test the `ping` endpoint response
        :return:
        """
        response = self.client.get("/ping")
        self.assertEqual(response.json, dict(ping='pong'))

    def test_rest_system(self):
        """
        Test the `system` endpoint response
        This test is valid only for my development machine.
        :return:
        """
        response = self.client.get("/system")
        self.assertEqual(response.json,
                         {
                             'service': {
                                 'version': '1.0'
                             },
                             'system': {
                                 'machine': 'x86_64',
                                 'platform': 'Linux-4.4.0-104-generic-x86_64-with-Ubuntu-16.04-xenial',
                                 'version': '#127-Ubuntu SMP Mon Dec 11 12:16:42 UTC 2017',
                                 'python_version': ['3.5.2 (default, Sep 10 2016, 08:21:44) ', '[GCC 5.4.0 20160609]'],
                                 'system': 'Linux'
                             }
                         }
                         )

    def test_rest_service_version(self):
        """
        Another test for `system` endpoint, where we test the only variable that should expect to be the same no matter
        on what machine the application runs
        :return:
        """
        response = self.client.get("/system")
        self.assertEqual(response.json['service']['version'], '1.0')

    def test_rest_mediainfo(self):
        """
        Test parts of the `mediainfo` endpoint response and ensure the validity of the assertions
        :return:
        """
        response = self.client.get("/mediainfo/11497188/")
        self.assertEqual(response.json['title'], 'Samuel Beckett Bridge In Dublin, Ireland.')
        self.assertEqual(response.json['artistname'], 'Sideral')
        self.assertEqual(response.json['id'], 11497188)

    def test_assert_200(self):
        """
        Test the 200 status code
        :return:
        """
        self.assert200(self.client.get("/"))

    def test_assert_400(self):
        """
        Test the 400 `Bad Request` status code. Unfortunately the mediainfo endpoint triggers pretty often this status
        code. It is dependent on the external source(Pond5 server response) which is the cause of it.
        :return:
        """
        self.assert400(self.client.get("/mediainfo/11497188"))

    def test_assert_401(self):
        """
        Test the 401 `Unauthorized` status code
        :return:
        """
        self.assert401(self.client.get("/unauthorized"))

    def test_assert_403(self):
        """
        Test the 403 `Forbidden` status code
        :return:
        """
        self.assert403(self.client.get("/forbidden"))

    def test_assert_404(self):
        """
        Test the 404 `Not Found` status code
        :return:
        """
        self.assert404(self.client.get("/oops"))

    def test_assert_405(self):
        """
        Test the 405 `Method Not Allowed` status code
        :return:
        """
        self.assert405(self.client.post("/"))

    def test_assert_500(self):
        """
        Test the 500 `Internal Server Error` status code
        :return:
        """
        self.assert500(self.client.get("/internal_server_error"))


if __name__ == '__main__':
    unittest.main()
