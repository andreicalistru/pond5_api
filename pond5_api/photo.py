import json
import requests
from lxml import html

POND_DOMAIN = 'https://www.pond5.com/photo/{}'
MEDIA_INFO_XPATH = '//div[contains(@item_id,"{}")]'
MEDIA_INFO_FORMAT_TYPES_XPATH = '//input[contains(@class,"MediaFormat-radio")]'
NOT_AVAILABLE_XPATH = '//div[contains(@class,"itemNotAvailable")]'


class PhotoException(Exception):
    pass


class Photo(object):
    def __init__(self, photo_id):
        self.photo_id = photo_id
        self.tree = None
        self.media_info = dict()
        self.media_format_types = dict()

    def get_photo_response(self):
        """
        Retrieve the html content of a Pond5 photo url
        :return: html content
        """
        headers = {'authority': 'www.pond5.com', 'method': 'GET', 'scheme': 'https',
                   'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}
        response = requests.get(POND_DOMAIN.format(self.photo_id), headers=headers)
        response.raise_for_status()
        return response.content

    def get_media_info(self):
        """
        Return the media info details
        :return: dictionary with media info or raise exception in case of failure
        """
        if len(self.tree.xpath(MEDIA_INFO_XPATH.format(self.photo_id))) > 0:
            media_info = json.loads(self.tree.xpath(MEDIA_INFO_XPATH.format(self.photo_id))[0].attrib['formats_data'])
        else:
            if len(self.tree.xpath(NOT_AVAILABLE_XPATH)) > 0:
                """The pond5 site actually returns 200 with a message 'Oh no! It appears this item is no longer 
                available on Pond5. Try searching again to find something else you might like.'. A status 404 might have
                been more suitable"""
                raise requests.HTTPError('404 - File not found')
            else:
                """Failed to match the media info details for another reason"""
                raise PhotoException('Unable to fetch media info for unknown reason')

        return media_info

    def get_media_format_types(self):
        """
        Returns the available formats of a photo
        :return: dictionary with available `format_types` of a photo
        """
        format_types = dict()
        elements = self.tree.xpath(MEDIA_INFO_FORMAT_TYPES_XPATH)
        if len(elements) > 0:
            for elem in elements:
                details = json.loads(elem.attrib['data-p5-format'])
                format_types[details['title']] = self.as_dict(details, 'format_types')

        return format_types

    def set_photo_details(self):
        """
        Enrich the Photo Object with the requested information
        :return: None
        """
        content = self.get_photo_response()
        self.tree = html.document_fromstring(content)
        self.media_info = self.get_media_info()
        self.media_info['format_types'] = dict()
        self.media_format_types = self.get_media_format_types()
        if self.media_format_types:
            self.media_info['format_types'] = self.media_format_types
        return None

    def as_dict(self, values=None, keys_type='info'):
        """
        Returns only the requested info about the provided dictionary
        :param values: input dictionary to be processed
        :param keys_type: string that tells what keys to be processed from the `values` dictionary
        :return: filtered dictionary containing only the keys requested
        """
        display_keys = {
            'info': ['artistname', 'id', 'title', 'format_types'],
            'format_types': ['size', 'width', 'resolution'],
            'full_info': [k for k in self.media_info]
        }

        if not values:
            values = self.media_info

        dict_repr = {k: values[k] for k in values if k in display_keys[keys_type]}
        return dict_repr
