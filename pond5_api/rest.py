import platform
import sys
import pkg_resources
import uuid

from flask import Flask, request, url_for, jsonify, make_response, current_app
from werkzeug.debug import get_current_traceback

from pond5_api.photo import *

__author__ = 'Andrei Calistru'

entry_points = {
    'ping': {'msg': 'Check it out'},
    'system': {'msg': 'Provides service version and system information'},
    'mediainfo': {'msg': 'Followed by an integer key, displays Pond5 media info, e.g. 11497188', 'key': 11497188}
}


def create_app():
    app = Flask(__name__)
    app.url_map.strict_slashes = False

    def entry_repr(key):
        return {
            'url': request.host_url.rstrip('/') + url_for(
                'rest_' + key,
                key=entry_points[key]['key'] if 'key' in entry_points[key] else None
            ),
            'details': entry_points[key]['msg']
        }

    def get_request_ip():
        if 'X-Forwarded-For' in request.headers:
            remote_addr = request.headers.getlist("X-Forwarded-For")[0].rpartition(' ')[-1]
        else:
            remote_addr = request.remote_addr or 'untrackable'

        return remote_addr

    """
    Error Handlers
    """
    @app.errorhandler(400)
    def error_bad_request():
        return make_response(jsonify({'error': 'Bad request', 'success': 'false'}), 400)

    @app.errorhandler(401)
    def error_not_found(error):
        return make_response(jsonify({'error': 'Unauthorized', 'success': 'false'}), 401)

    @app.errorhandler(403)
    def error_forbidden(error):
        return make_response(jsonify({'error': 'Forbidden', 'success': 'false'}), 403)

    @app.errorhandler(404)
    def error_resource_not_found(error):
        return make_response(jsonify({'error': 'Resource not found', 'success': 'false'}), 404)

    @app.errorhandler(405)
    def error_method_not_allowed(error):
        return make_response(jsonify({'error': 'Method not allowed', 'success': 'false'}), 405)

    @app.errorhandler(500)
    def error_internal_server_error(error):
        return make_response(jsonify({'error': 'Internal server error', 'success': 'false'}), 500)

    @app.errorhandler(Exception)
    def error_unhandled_exception(error):
        error_uuid = str(uuid.uuid4())
        track = get_current_traceback(skip=1, show_hidden_frames=True, ignore_system_exceptions=False)
        error_message = """[%s] Unhandled Exception [%s]: %s
                Path: %s
                Data: %s
                %s'
            """ % (get_request_ip(),
                   error_uuid,
                   error,
                   request.path,
                   request.data,
                   track.plaintext)

        current_app.logger.error(error_message)
        return make_response(jsonify({'error': 'Internal server error', 'success': 'false', 'error_code': error_uuid}), 500)

    """
    Routes
    """
    @app.route("/", methods=['GET'])
    def rest_entry_points_list():
        """
        List available entry points
        """

        return make_response(jsonify({idx: entry_repr(idx) for idx in sorted(entry_points.keys())}))

    @app.route("/ping", methods=['GET'])
    def rest_ping():
        """
        Return Pong
        """

        return make_response(jsonify(ping="pong"))

    @app.route("/system", methods=['GET'])
    def rest_system():
        """
        Return Service Version and System Information
        """
        system = {
            'service': {
                'version': pkg_resources.require("pond5_api")[0].version,
            },
            'system': {
                'python_version': sys.version.split('\n'),
                'system': platform.system(),
                'machine': platform.machine(),
                'platform': platform.platform(),
                'version': platform.version()
            }
        }

        return make_response(jsonify(system))

    @app.route("/mediainfo/<int:key>", methods=['GET'])
    def rest_mediainfo(key):
        """
        Return Preview Information for a Pond5 image
        """
        photo = Photo(key)
        photo.set_photo_details()
        media_info = photo.as_dict()

        return jsonify(media_info)

    """
    Test purpose routes for Error Handling display messages
    """
    @app.route("/unauthorized")
    def rest_unauthorized():
        return jsonify(error='Unauthorized'), 401

    @app.route("/forbidden")
    def rest_forbidden():
        return jsonify(error='Forbidden'), 403

    @app.route("/oops")
    def rest_bad_url():
        return jsonify(error='Not Found'), 404

    @app.route("/internal_server_error")
    def rest_internal_server_error():
        return jsonify(error='Internal server error'), 500

    return app


def main():
    app = create_app()
    return app.run(host='0.0.0.0', debug=True)


if __name__ == "__main__":
    main()
