__author__ = "Andrei Calistru"
from setuptools import setup

setup(name="pond5_api",
      version="1.0",
      description="Pond 5 REST Api",
      long_description="""This is a Flask implementation of a Pond5 REST API endpoint""",
      classifiers=[
          "Development Status :: 1 - Pre-Alpha",
          "Environment :: Console",
          "Natural Language :: English",
          "Operating System :: POSIX :: Linux",
          "Programming Language :: Python :: 3.6",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: Browsers",
          "Topic :: Text Processing :: General",
      ],
      author="Andrei Calistru",
      author_email="andrei.calistru@gmail.com",
      url="https://bitbucket.org/andreicalistru/pond5-api",
      license="GPL",
      install_requires=["requests", "lxml", "flask", "Flask-Testing"]
      )
